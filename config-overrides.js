const rewireStyl = require("./stylus.rules");

module.exports = function override(config, env) {
    config = rewireStyl(config, env);
    return config;
};
