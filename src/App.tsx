import { AppLayout } from 'common/components';
import React, { useEffect } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { AppRoutes } from 'routes';
import { IntlProvider } from 'rsuite';
import 'rsuite/dist/styles/rsuite-default.css';
import ruRU from 'rsuite/lib/IntlProvider/locales/ru_RU';
import { authService } from 'services';
import { userFiltersService } from 'services/user-filters.service';

export const App: React.FC = () => {
  useEffect(() => {
    authService.startMonitoringUser();
    userFiltersService.loadFilters();

    return () => {
      authService.stopMonitoringUser();
    };
  }, []);

  return (
    <IntlProvider locale={ruRU}>
      <Router>
        <div className="App">
          <AppLayout>
            <AppRoutes/>
          </AppLayout>
        </div>
      </Router>
    </IntlProvider>

  );
};
