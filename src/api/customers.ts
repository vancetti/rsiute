import {
  Customer,
  CustomerServiceClient,
  GetCustomersByIdsRequest,
  GetCustomersByIdsResponse,
  ListCustomerResponse,
  ReadCustomerRequest,
  ReadCustomerResponse,
  SearchCustomerRequest,
  UpdateCustomerRequest,
  UpdateCustomerResponse
} from '@tochka-modules/ved-proto';
import { fetchData, grpcToPromise, makeFieldMask, RPC_OPTIONS, SERVICE_HOST, TApiResponse } from 'api/grpc';

const serviceClient = new CustomerServiceClient(SERVICE_HOST, RPC_OPTIONS);

export function getCustomersByIds(ids: Array<number>): TApiResponse<GetCustomersByIdsResponse> {
  const data = new GetCustomersByIdsRequest();
  const transport = grpcToPromise<GetCustomersByIdsRequest, GetCustomersByIdsResponse>(
    serviceClient.getCustomerByIds.bind(serviceClient)
  );
  data.setIdsList(ids);

  return fetchData({ transport, data });
}

export function searchCustomers(value: string = ''): TApiResponse<ListCustomerResponse> {
  const req = new SearchCustomerRequest();

  req.setQuery(value);

  const search = grpcToPromise<SearchCustomerRequest, ListCustomerResponse>(
    serviceClient.search.bind(serviceClient)
  );

  return fetchData({
    transport: search,
    data: req
  });
}

export async function getCustomer(colvirId: number): Promise<Customer> {
  const data = new ReadCustomerRequest();
  data.setId(colvirId);

  const { response } = await fetchData({
    transport: grpcToPromise<ReadCustomerRequest, ReadCustomerResponse>(
      serviceClient.read.bind(serviceClient)
    ),
    data
  });

  if (response) {
    return response.getResult() || new Customer();
  }
  return new Customer();
}

export function updateComplianceById(id: number, compliance: string): TApiResponse<UpdateCustomerResponse> {
  const customer = new Customer();
  customer.setId(id);
  customer.setCompliance(compliance);

  const query = new UpdateCustomerRequest();
  query.setPayload(customer);
  query.setPatchFields(makeFieldMask('Compliance'));

  return fetchData({
    transport: grpcToPromise<UpdateCustomerRequest, UpdateCustomerResponse>(
      serviceClient.update.bind(serviceClient)
    ),
    data: query
  });
}

export function updateVedAssistant(id: number, vedAssistant: boolean): TApiResponse<UpdateCustomerResponse> {
  const customer = new Customer();
  customer.setId(id);
  customer.setVedAssistant(vedAssistant);

  const query = new UpdateCustomerRequest();
  query.setPayload(customer);
  query.setPatchFields(makeFieldMask('VedAssistant'));

  return fetchData({
    transport: grpcToPromise<UpdateCustomerRequest, UpdateCustomerResponse>(
      serviceClient.update.bind(serviceClient)
    ),
    data: query
  });
}

export function updateVip(id: number, vip: boolean): TApiResponse<UpdateCustomerResponse> {
  const customer = new Customer();
  customer.setId(id);
  customer.setVip(vip);

  const query = new UpdateCustomerRequest();
  query.setPayload(customer);
  query.setPatchFields(makeFieldMask('Vip'));

  return fetchData({
    transport: grpcToPromise<UpdateCustomerRequest, UpdateCustomerResponse>(
      serviceClient.update.bind(serviceClient)
    ),
    data: query
  });
}
