import {
  CreateUserFilterRequest,
  CreateUserFilterResponse,
  DeleteUserFilterRequest,
  DeleteUserFilterResponse,
  Filtering,
  ListUserFilterRequest,
  ListUserFilterResponse,
  NumberCondition,
  UpdateUserFilterRequest,
  UpdateUserFilterResponse,
  UserFilter,
  UserFilterServiceClient
} from '@tochka-modules/ved-proto';
import { fetchData, grpcToPromise, RPC_OPTIONS, SERVICE_HOST, TApiResponse } from 'api/grpc';

const serviceClient = new UserFilterServiceClient(SERVICE_HOST, RPC_OPTIONS);

export function getFiltersByUser(userId: number): TApiResponse<ListUserFilterResponse> {
  const numberCondition = new NumberCondition();
  numberCondition.setFieldPathList(['user_id']);
  numberCondition.setValue(userId);
  numberCondition.setType(NumberCondition.Type.EQ);

  const filtering = new Filtering();
  filtering.setNumberCondition(numberCondition);

  const query = new ListUserFilterRequest();
  query.setFilter(filtering);

  return fetchData({
    transport: grpcToPromise<ListUserFilterRequest, ListUserFilterResponse>(
      serviceClient.list.bind(serviceClient)
    ),
    data: query
  });
}

export function createUserFilter(userId: number): TApiResponse<CreateUserFilterResponse> {
  const userFilter = new UserFilter();
  userFilter.setUserId(userId);
  userFilter.setName('Введите название фильтра');

  const query = new CreateUserFilterRequest();
  query.setPayload(userFilter);

  return fetchData({
    transport: grpcToPromise<CreateUserFilterRequest, CreateUserFilterResponse>(
      serviceClient.create.bind(serviceClient)
    ),
    data: query
  });
}

export function updateFilter(filterToUpdate: UserFilter): TApiResponse<UpdateUserFilterResponse> {
  const query = new UpdateUserFilterRequest();
  query.setPayload(filterToUpdate);

  return fetchData({
    transport: grpcToPromise<UpdateUserFilterRequest, UpdateUserFilterResponse>(
      serviceClient.update.bind(serviceClient)
    ),
    data: query
  });
}

export function deleteFilterById(filterId: number): TApiResponse<DeleteUserFilterResponse> {
  const query = new DeleteUserFilterRequest();
  query.setId(filterId);

  return fetchData({
    transport: grpcToPromise<DeleteUserFilterRequest, DeleteUserFilterResponse>(
      serviceClient.delete.bind(serviceClient)
    ),
    data: query
  });
}
