import { RpcOptions } from '@improbable-eng/grpc-web/dist/typings/client';

export const SERVICE_HOST = '/api/v2';

export const RPC_OPTIONS: RpcOptions = {
  debug: process.env.VUE_APP_GRPC_WEB_DEBUG === 'true'
};
