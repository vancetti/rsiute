import { User } from '@tochka-modules/ved-proto/stash.bank24.int/ved/supreme/proto/user_pb';
import { UserServiceClient } from '@tochka-modules/ved-proto/stash.bank24.int/ved/supreme/proto/user_pb_service';
import { fetchData, grpcToPromise, IFetchDataResponse, SERVICE_HOST } from 'api/grpc';
import { Empty } from 'google-protobuf/google/protobuf/empty_pb';

const service = new UserServiceClient(SERVICE_HOST);

export function loadUser(): Promise<IFetchDataResponse<User>> {
  return fetchData({
    data: new Empty(),
    showInformer: false,
    transport: grpcToPromise<Empty, User>(service.getUser.bind(service))
  });
}
