import { CustomerSearch } from 'common/components/customer-search';
import React from 'react';
import { FlexboxGrid, Header, Navbar } from 'rsuite';

export function AppHeader(): JSX.Element {
  return (
    <Header>
      <Navbar appearance="inverse" style={{ padding: '0 20px' }}>
        <FlexboxGrid justify="space-between" align="middle">
          <FlexboxGrid.Item colspan={4}>
            <Navbar.Header style={{ display: 'flex', alignItems: 'center' }}>
              <span style={{ fontSize: '18px', fontWeight: 500, cursor: 'pointer' }}>SUPREME 2.0</span>
            </Navbar.Header>
          </FlexboxGrid.Item>

          <FlexboxGrid.Item colspan={20}>
            <CustomerSearch/>
          </FlexboxGrid.Item>
        </FlexboxGrid>
      </Navbar>
    </Header>
  );
}
