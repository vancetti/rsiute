import { AppHeader } from 'common/components/app-header';
import { AppSidebar } from 'common/components/app-sidebar';
import React, { ReactNode } from 'react';
import { Container, Content } from 'rsuite';

export function AppLayout({ children }: { children?: ReactNode }): JSX.Element {
  return (
    <Container>
      <AppHeader/>
      <Container>
        <AppSidebar/>
        <Content style={{ padding: 20 }}>
          {children}
        </Content>
      </Container>
    </Container>
  );
}
