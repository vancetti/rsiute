import { TPopupState } from 'common/components/app-modal-manager/popup.types';
import React from 'react';
import { BehaviorSubject, Observable } from 'rxjs';

/**
 * Сервис для открытия попапов
 */
export class PopupService {
  private _currentPopupState$: BehaviorSubject<TPopupState> = new BehaviorSubject<TPopupState>({ show: false });
  private _stateStack: Array<TPopupState> = [];

  /** @internal */
  currentPopup$: Observable<TPopupState> = this._currentPopupState$.asObservable();

  /**
   * Открыть новый попап, все предыдущие при этом закрываются
   */
  open = (component: React.ReactNode): void => {
    this._stateStack.length = 0;
    this._currentPopupState$.next({ component, show: true });
  };

  /**
   * Открыть новый попап, при этом сохранив предыдущий, при закрытии этого откроется прошлый
   */
  openChained = (component: React.ReactNode): void => {
    const current = this._currentPopupState$.value;
    if (current.show) {
      this._stateStack.push(current);
    }
    this._currentPopupState$.next({ component, show: true });
  };

  /**
   * Закрыть текущий попап, в случае, если он был открыт с помощью openChained, откроется предыдущий
   */
  close = (): void => {
    const stacked = this._stateStack.pop();
    const nextState: TPopupState = stacked ? stacked : { show: false };
    this._currentPopupState$.next(nextState);
  };

  /**
   * Закрыть все попапы
   */
  closeAll = (): void => {
    this._stateStack.length = 0;
    this._currentPopupState$.next({ show: false });
  };
}

export const popupService = new PopupService();
