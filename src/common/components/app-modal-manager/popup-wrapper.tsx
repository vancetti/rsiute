import { popupService } from 'common/components/app-modal-manager/popup-service';
import { TPopupWrapperProps } from 'common/components/app-modal-manager/popup.types';
import { useObservable } from 'utils/hooks';

export function PopupWrapper(props: TPopupWrapperProps): JSX.Element | null {
  const current = useObservable(popupService.currentPopup$);

  if (!current || !current.show) {
    return props.children({
      children: null,
      isOpen: false
    });
  }

  return props.children({});
}
