import React from 'react';

export type TPopupWrapperProps = {
  children: (props: Readonly<any>) => JSX.Element;
};

export type TPopupState = {
  show: false;
} | {
  show: true;
  component: React.ReactNode;
};
