import { IAppSidebarItem } from 'common/components/app-sidebar/app-sidebar.const';
import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { Icon, Nav } from 'rsuite';

/**
 *  компонент для рендера навигационной ссылки
 *  используется в сайдбаре и для перехода по вкладкам кастомера
 */
export function AppNavLink(props: IAppSidebarItem): JSX.Element {
  const { icon, path, title, parentRoute } = props;
  const location = useLocation();

  const isParentRoute = !!parentRoute && parentRoute === location.pathname.split('/')[1];

  const isActive: boolean = location.pathname === path || isParentRoute;

  return (
    <Nav.Item
      componentClass={Link}
      active={isActive}
      to={path}
      icon={icon ? <Icon icon={icon}/> : undefined}
    >
      {title}
    </Nav.Item>
  );
}
