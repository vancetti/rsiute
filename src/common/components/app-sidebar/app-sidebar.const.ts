import { SVGIcon } from 'rsuite/lib/@types/common';
import { IconNames } from 'rsuite/lib/Icon';

export interface IAppSidebarItem {
  title: string;
  icon?: IconNames | SVGIcon;
  path: string;
  parentRoute?: string;
}

// статичные роуты приложения
export const appSidebarItems: Array<IAppSidebarItem> = [
    {
        title: 'Таймлайн',
        icon: 'dashboard',
        path: '/'
    },
    {
        title: 'Пользователь',
        icon: 'beer',
        path: '/user'
    }
];
