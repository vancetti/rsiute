import { AppNavLink } from 'common/components/app-nav-link/app-nav-link.component';
import { appSidebarItems, IAppSidebarItem } from 'common/components/app-sidebar/app-sidebar.const';
import React, { useState } from 'react';
import { Icon, Nav, Navbar, Sidebar, Sidenav } from 'rsuite';
import { lastCustomersService } from 'services/last-customers.service';
import { useObservable } from 'utils/hooks';

interface INavToggleProps {
  isExpanded: boolean;
  onChange: () => void;
}

const NavToggle = ({ isExpanded, onChange }: INavToggleProps): JSX.Element => {
  return (
    <Navbar className="nav-toggle">
      <Navbar.Body>
        <Nav pullRight>
          <Nav.Item onClick={onChange} style={{ width: 56, textAlign: 'center' }}>
            <Icon icon={isExpanded ? 'angle-left' : 'angle-right'}/>
          </Nav.Item>
        </Nav>
      </Navbar.Body>
    </Navbar>
  );
};

export function AppSidebar(): JSX.Element {
  const [isExpanded, handleExpand] = useState(true);

  const lastCustomerId = useObservable(lastCustomersService.lastCustomerId$);

  const handleChange = (): void => {
    handleExpand(!isExpanded);
  };

  return (
    <Sidebar
      style={{ display: 'flex', flexDirection: 'column' }}
      width={isExpanded ? 260 : 56}
      collapsible
    >
      <Sidenav expanded={isExpanded}>
        <Sidenav.Body>
          <Nav>
            {appSidebarItems.map((item: IAppSidebarItem) => (
              <AppNavLink
                key={item.title}
                path={item.path}
                icon={item.icon}
                title={item.title}
                parentRoute={item.parentRoute}
              />
            ))}

            <AppNavLink
              path={`/customer/${lastCustomerId}/contracts`}
              icon="user"
              parentRoute="customer"
              title="Клиент"
            />
          </Nav>
        </Sidenav.Body>
      </Sidenav>
      <NavToggle isExpanded={isExpanded} onChange={handleChange}/>
    </Sidebar>
  );
}
