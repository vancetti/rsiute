import {
    Customer,
    ListCustomerResponse,
    ReadCustomerRequest,
    ReadCustomerResponse,
    SearchCustomerRequest
} from '@tochka-modules/ved-proto/stash.bank24.int/ved/supreme/proto/customer_pb';
import { CustomerServiceClient } from '@tochka-modules/ved-proto/stash.bank24.int/ved/supreme/proto/customer_pb_service';
import { TApiResponse } from 'api/grpc';
import { SERVICE_HOST } from 'utils/grpc/grpc.const';
import { fetchData, grpcToPromise } from 'utils/grpc/grpcToPromise';

const serviceClient = new CustomerServiceClient(SERVICE_HOST);

export async function getCustomerById(colvirId: string): Promise<Customer.AsObject> {
    const data = new ReadCustomerRequest();
    data.setId(Number(colvirId));

    const { response } = await fetchData({
        transport: grpcToPromise<ReadCustomerRequest, ReadCustomerResponse>(
            serviceClient.read.bind(serviceClient)
        ),
        data
    });
    const customerResult: Customer | undefined = response && response.getResult();

    return customerResult ? customerResult.toObject() : new Customer().toObject();
}

export function searchCustomers(value: string = ''): TApiResponse<ListCustomerResponse> {
    const req = new SearchCustomerRequest();

    req.setQuery(value);

    const search = grpcToPromise<SearchCustomerRequest, ListCustomerResponse>(
        serviceClient.search.bind(serviceClient)
    );

    return fetchData({
        transport: search,
        data: req
    });
}
