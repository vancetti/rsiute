import { Customer } from '@tochka-modules/ved-proto/stash.bank24.int/ved/supreme/proto/customer_pb';
import { customerSearchService } from 'common/components/customer-search/customer-search.service';
import Downshift, { ControllerStateAndHelpers } from 'downshift';
import React, { ChangeEvent, RefObject, useEffect, useRef, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Icon, InputGroup, Message } from 'rsuite';
import { lastCustomersService } from 'services/last-customers.service';
import { useObservable } from 'utils/hooks';
import { prettifyCustomerName } from 'utils/prettify-customer-name';
import './customer-search.component.css';

// function customersToDataItemType(customers: Array<Customer.AsObject>): Array<TDataItemType> {
//     return customers.map((customer: Customer.AsObject) => ({
//         label: customer.fullName,
//         value: customer.id + ''
//     }));
// }
//
// function renderCustomerSearchItems(item: Customer.AsObject): string {
//     return item.fullName;
// }

function customerToString(customer: Customer.AsObject | null): string {
  return customer ? customer.fullName : '';
}

export function CustomerSearch(): JSX.Element {
  const { searchString$, loadedCustomers$ } = customerSearchService;
  const { lastCustomers$ } = lastCustomersService;
  const [searchString, updateSearchString] = useState('');
  const [selectedCustomer, setSelectedCustomer] = useState<Customer.AsObject | null>(null);
  const lastCustomers = useObservable(lastCustomers$);
  const loadedCustomers = useObservable(loadedCustomers$);
  const history = useHistory();

  const ref: RefObject<Downshift> = useRef<Downshift>(null);

  useEffect(() => {
    const subscription = customerSearchService.resetInput$
      .subscribe(() => {
        updateSearchString('');
        // @ts-ignore
        ref.current && ref.current.clearSelection();
      });
    return () => subscription.unsubscribe();
  }, []);

  const loadedData = searchString ? loadedCustomers : lastCustomers;

  // const isLoading = useObservable(isLoading$);

  useEffect(() => {
    lastCustomersService.startMonitoring();

    return () => lastCustomersService.stopMonitoring();
  }, []);

  const handleSelect = (selectedValue: Customer.AsObject | null) => {
    if (!selectedValue || !selectedValue.id) return;
    setSelectedCustomer(selectedValue);

    lastCustomersService.setLastCustomerId(selectedValue.id);
    history.push(`/customer/${selectedValue.id}/contracts`);
  };

  const isSomethingSelected = (selectedCustomer || searchString);
  const iconName = isSomethingSelected ? 'close-circle' : 'search';

  const handleIconClick = (): void => {
    if (isSomethingSelected) customerSearchService.resetInput$.next();
  };

  return (
    <Downshift
      ref={ref}
      itemToString={customerToString}
      onSelect={handleSelect}
    >
      {({
          isOpen,
          openMenu,
          getItemProps,
          getInputProps,
          selectedItem
      }: ControllerStateAndHelpers<Customer.AsObject>): JSX.Element => {
                const handleInputClick = (): void => {
                    openMenu();
                };

                const hasMenuItems = !!(loadedData && loadedData.length);
                const emptyItems = loadedData && !loadedData.length;

                const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
                    // inputValue = e.target.value;
                    searchString$.next(e.target.value);
                    updateSearchString(e.target.value);
                };

                return (
                    <div style={{ position: 'relative' }}>
                        <InputGroup inside>
                            <input
                                className="rs-input"
                                {...getInputProps({
                                    placeholder: 'Искать по имени, коду, инн...',
                                    onChange: handleInputChange,
                                    onClick: handleInputClick
                                })}
                            />
                            <InputGroup.Addon>
                                <Icon style={{ cursor: 'pointer' }} icon={iconName} onClick={handleIconClick}/>
                            </InputGroup.Addon>
                        </InputGroup>

                        {isOpen && <div className="customer-search__dropdown-menu">
                            {hasMenuItems && (loadedData as Array<Customer.AsObject>).map((item: Customer.AsObject, index: number) => {
                                return (
                                    <div
                                        {...getItemProps({
                                            key: item.id,
                                            index,
                                            item
                                        })}
                                        key={item.id}
                                        className="customer-search__dropdown-item"
                                    >
                                        <div>{prettifyCustomerName(item.fullName)}</div>
                                        <div className="customer-search__dropdown-item--subtitle">
                                            ИНН: {item.inn},
                                            АБС: {item.id}
                                        </div>
                                    </div>
                                );
                            })
                            }
                            {emptyItems && <Message type="error" description="Ничего не найдено =("/>}
                        </div>}
                    </div>
                );
            }}
    </Downshift>
  );
}
