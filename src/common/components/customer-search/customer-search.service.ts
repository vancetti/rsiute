import {
  Customer,
  ListCustomerResponse
} from '@tochka-modules/ved-proto/stash.bank24.int/ved/supreme/proto/customer_pb';
import { searchCustomers } from 'common/components/customer-search/customer-search.api';
import { TDataItemType } from 'common/interfaces';
import { defer, from, Observable, Subject } from 'rxjs';
import { debounceTime, filter, map, switchMap, tap } from 'rxjs/operators';

class CustomerSearchService {
  searchString$: Subject<string> = new Subject();
  isLoading$: Subject<boolean> = new Subject();

  loadedCustomers$: Observable<Array<Customer.AsObject>>;
  loadedData$: Observable<Array<TDataItemType>>;

  resetInput$: Subject<void> = new Subject();

  private static customersToDataItemType(customers: Array<Customer.AsObject>): Array<TDataItemType> {
    return customers.map((customer: Customer.AsObject) => ({
      label: customer.fullName,
      value: customer.id + ''
    }));
  }

  constructor() {
    this.loadedCustomers$ = this.searchString$
      .pipe(
        filter((val: string) => !!val),
        tap(() => this.isLoading$.next(true)),
        debounceTime(300),
        map((search: string) => search.trim()),
        switchMap((search: string) => defer(() => from(searchCustomers(search)))),
        filter(rawResponse => !!rawResponse.response),
        map(rawResponse => (rawResponse.response as ListCustomerResponse).toObject().resultsList)
      );

    this.loadedData$ = this.loadedCustomers$.pipe(map(CustomerSearchService.customersToDataItemType));
  }
}

export const customerSearchService = new CustomerSearchService();
