import React, { useState } from 'react';
import { HelpBlock, Input } from 'rsuite';
import { InputProps } from 'rsuite/lib/Input';

import './input-counter.component.styl';

interface IInputCounterProps {
    inputProps?: InputProps;
    maxCounter: number;
}

export function InputCounter(props: IInputCounterProps): JSX.Element {
    const { inputProps, maxCounter } = props;
    const [counter, setCounter] = useState(0);
    const [inputValue, setInputValue] = useState(inputProps ? inputProps.value : '');

    const handleChange = (value: string, event: React.SyntheticEvent<HTMLElement>) => {
        if (value.length > maxCounter) return;

        setCounter(value.length);
        setInputValue(value);

        if (inputProps && inputProps.onChange) inputProps.onChange(value, event);
    };
    return (
        <>
            <Input
                {...inputProps}
                value={inputValue}
                onChange={handleChange}
            />
            <HelpBlock className="input-counter__counter">{counter}/{props.maxCounter}</HelpBlock>
        </>
    );
}
