import { ReactNode } from 'react';

export type TDataItemType = {
    value: string; // property value is the value of valueKey
    label: ReactNode; // property value is the value of labelKey
    children?: Array<TDataItemType>; // property value is the value of childrenKey
    groupBy?: string;
};
