import React from 'react';

interface ICustomerDocumentsProps {
  isIncoming: boolean;
}

export function CustomerDocuments({ isIncoming }: ICustomerDocumentsProps): JSX.Element {
  return (
    <div>customer {isIncoming ? 'incoming' : 'outgoing'} documents</div>
  );
}
