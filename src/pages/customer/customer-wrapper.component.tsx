import { popupService } from 'common/components/app-modal-manager';
import { AppNavLink } from 'common/components/app-nav-link/app-nav-link.component';
import { CustomerDeals } from 'pages/customer/customer-deals';
import { CustomerDocuments } from 'pages/customer/customer-documents';
import { renderLogos } from 'pages/customer/customer-wrapper.const';
import React from 'react';
import { Route, RouteComponentProps, Switch, useRouteMatch } from 'react-router';
import { withRouter } from 'react-router-dom';
import { Button, FlexboxGrid, InputGroup, Nav, Panel, Toggle } from 'rsuite';
import { useCustomer } from 'utils/hooks/use-customer';
import { prettifyCustomerName } from 'utils/prettify-customer-name';
import { CustomerContracts } from './customer-contracts';

interface ICustomerPageProps {
  id: string;
}

function openCustomerInfo(): void {
  popupService.open(
    <div>
      <Button onClick={popupService.close}>Закрыть</Button>
    </div>
  );
}

function renderIncomingDocuments(): JSX.Element {
  return <CustomerDocuments isIncoming={true}/>;
}

function renderOutgoingDocuments(): JSX.Element {
  return <CustomerDocuments isIncoming={false}/>;
}

function CustomerWrapper(props: RouteComponentProps<ICustomerPageProps>): JSX.Element {
  const { url } = useRouteMatch();

  const customer = useCustomer(props.match.params.id);
  const customerName = prettifyCustomerName(customer.fullName);

  return (
    <>
      <Panel shaded>
        <FlexboxGrid align="middle" justify="space-between">
          <FlexboxGrid.Item>
            <FlexboxGrid align="middle">
              <FlexboxGrid.Item style={{ marginRight: 20 }}>
                {customerName} {renderLogos(customer)}
              </FlexboxGrid.Item>

              <FlexboxGrid.Item style={{ marginRight: 20 }}>
                <Button
                  onClick={openCustomerInfo}
                  appearance="ghost"
                >
                  Подробнее
                </Button>
              </FlexboxGrid.Item>

              <FlexboxGrid.Item style={{ marginRight: 20 }}>
                <Button appearance="ghost">Создать новый контракт</Button>
              </FlexboxGrid.Item>

              <FlexboxGrid.Item>
                <Button appearance="ghost">Создать новый платёж</Button>
              </FlexboxGrid.Item>
            </FlexboxGrid>
          </FlexboxGrid.Item>

          <FlexboxGrid.Item>
            <InputGroup inside>
              <input placeholder="Поиск в контрактах"/>
            </InputGroup>
            <div>
              <Toggle size="md"/>
            </div>
          </FlexboxGrid.Item>
        </FlexboxGrid>
      </Panel>

      <Nav style={{ margin: '20px 0' }}>
        <AppNavLink
          path={`${url}/contracts`}
          title="Контракты"
        />

        <AppNavLink
          path={`${url}/incomingDocuments`}
          title="Входящие документы"
        />

        <AppNavLink
          path={`${url}/outgoingDocuments`}
          title="Исходящие документы"
        />

        <AppNavLink
          path={`${url}/deals`}
          title="Сделки"
        />
      </Nav>

      <Panel shaded>
        <Switch>
          <Route component={CustomerContracts} path={`${url}/contracts`}/>
          <Route
            path={`${url}/incomingDocuments`}
            render={renderIncomingDocuments}
          />

          <Route
            path={`${url}/outgoingDocuments`}
            render={renderOutgoingDocuments}
          />

          <Route component={CustomerDeals} path={`${url}/deals`}/>
        </Switch>
      </Panel>
    </>

  );
}

export const CustomerPage = withRouter(CustomerWrapper);
