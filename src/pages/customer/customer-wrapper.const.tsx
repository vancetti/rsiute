import { Customer } from '@tochka-modules/ved-proto/stash.bank24.int/ved/supreme/proto/customer_pb';
import React from 'react';

const iconPrefix = process.env.PUBLIC_URL + '/img/icons/';

export function renderLogos(customer: Customer.AsObject): JSX.Element {
  const { settlementBank, vedAssistant, vip } = customer;
  const logos: Array<string> = [];

  if (settlementBank && settlementBank.name === 'QIWI') {
    logos.push(iconPrefix + 'qiwi.png');
  } else if (settlementBank && settlementBank.name === 'OPEN') {
    logos.push(iconPrefix + 'open.svg');
  }
  if (vedAssistant) {
    logos.push(iconPrefix + 'assistant.svg');
  }
  if (vip) {
    logos.push(iconPrefix + 'vip.svg');
  }

  return (
    <>
      {logos.map((logoUrl: string) => (
        <img
          alt={'icon'}
          style={{ height: 24 }}
          key={logoUrl}
          src={logoUrl}
          className="additional-icon"
        />
      ))}
    </>
  );
}
