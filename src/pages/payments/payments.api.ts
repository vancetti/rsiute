import { Pagination } from '@tochka-modules/ved-proto/github.com/infobloxopen/atlas-app-toolkit/query/collection_operators_pb';
import { ByIdRequest } from '@tochka-modules/ved-proto/stash.bank24.int/ved/supreme/proto/deal_pb';
import { FilteredRequest } from '@tochka-modules/ved-proto/stash.bank24.int/ved/supreme/proto/timeline_pb';
import {
    IdRequest,
    PaymentEvent,
    PromoteVedPaymentRequest,
    VedPayment,
    VedPaymentListResponse
} from '@tochka-modules/ved-proto/stash.bank24.int/ved/supreme/proto/ved_payment_pb';
import { VedPaymentServiceClient } from '@tochka-modules/ved-proto/stash.bank24.int/ved/supreme/proto/ved_payment_pb_service';
import { IFilteredRequest } from 'pages/payments/payments.service';
import { RPC_OPTIONS, SERVICE_HOST } from 'utils/grpc/grpc.const';
import { fetchData, grpcToPromise, IFetchDataResponse } from 'utils/grpc/grpcToPromise';

type PromiseGRPC<T> = Promise<IFetchDataResponse<T>>;

const serviceClient = new VedPaymentServiceClient(SERVICE_HOST, RPC_OPTIONS);

export function getPaymentTimelineEvents(params: IFilteredRequest): PromiseGRPC<VedPaymentListResponse> {
    const data = new FilteredRequest();

    data.setFilter(params.userFilter);
    data.setIncludeGroup(params.group);
    data.setOnlyFree(params.empty);
    data.setQuery(params.search);
    data.setOnlyMine(params.isOnlyMine);

    const paging = new Pagination();
    paging.setLimit(20);
    paging.setOffset(params.offset || 0);
    data.setPaging(paging);

    return fetchData({
        transport: grpcToPromise<FilteredRequest, VedPaymentListResponse>(
            serviceClient.preparedVedPayments.bind(serviceClient)
        ),
        data
    });
}

export async function getPaymentEventById(id: string): Promise<PaymentEvent.AsObject | null> {
    const data = new IdRequest();
    data.setId(Number(id));

    try {
        const { response, isSuccess } = await fetchData({
            transport: grpcToPromise<ByIdRequest, PaymentEvent>(
              serviceClient.read.bind(serviceClient)
            ),
            data
        });

        if (!response || !isSuccess) return null;

        return response.toObject();
    } catch (e) {
        return null;
    }
}

export async function approvePayment(paymentId: number): Promise<void> {
    const data = new PromoteVedPaymentRequest();
    data.setPaymentId(paymentId);

    const { response } = await fetchData({
        transport: grpcToPromise<PromoteVedPaymentRequest, VedPayment>(
            serviceClient.confirmVedPayment.bind(serviceClient)
        ),
        data
    });
    console.debug(response);
}
