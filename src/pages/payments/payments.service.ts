import { UserFilter } from '@tochka-modules/ved-proto/stash.bank24.int/ved/supreme/proto/user_filter_pb';
import { PaymentEvent } from '@tochka-modules/ved-proto/stash.bank24.int/ved/supreme/proto/ved_payment_pb';
import _get from 'lodash.get';
import { BehaviorSubject, combineLatest, defer, Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, switchMap, tap } from 'rxjs/operators';
import { getPaymentTimelineEvents } from './payments.api';

export interface ILoadPaymentRequest {
    userFilter: UserFilter;
}

export type TLoadPaymentRequest = ILoadPaymentRequest & {
    offset: number;
    search: string;
};

export enum PaymentTypeEnum {
    NONRES = 'NONRES',
    TWO = 'TWO'
}

export interface IFilteredRequest {
    userFilter: UserFilter;
    group: boolean;
    empty: boolean;
    search: string;
    isOnlyMine: boolean;
    offset?: number;
}

export const PaymentTypeDict: Record<PaymentTypeEnum, string> = {
    [PaymentTypeEnum.NONRES]: 'Платёж нерезиденту',
    [PaymentTypeEnum.TWO]: 'Входящее распоряжение'
};

export interface IPaymentEvent {
    id: number;
    eventDate: string;
    customerName: string;
    isVip: boolean;
    isVedAssist: boolean;
    type: string;
    sum: string;
    dueDate: string;
    customerCode: string;
    newEvents: boolean;
    newDocuments: boolean;
    ticketText: string;
    assignedUser: string;
}

function BuildPaymentEvents(rawEvents: Array<PaymentEvent.AsObject>): Array<IPaymentEvent> {
  return rawEvents.map(event => {
    const customerName = _get(event, 'customer.fullName', '');
    const eventDate = event.eventDate ? event.eventDate.toString() : '';
    const isVip = _get(event, 'customer.isVip', false);
    const isVedAssist = _get(event, 'customer.isVedAssistant', false);

    const rawType: PaymentTypeEnum = _get(event, 'payment.type', '');
    const type = PaymentTypeDict[rawType];

    const sum = _get(event, 'payment.transactionSum', '') + ' ' + _get(event, 'payment.currency', '');
    const dueDate = _get(event, 'payment.dueDate', '');
    const customerCode = _get(event, 'customer.id', '');
    const ticketText = _get(event, 'deal.ticket.content', '');

    const assignedUser = '';
    const newEvents = false;
    const newDocuments = false;

    return {
      id: event.id,
      eventDate,
      customerName,
      isVip,
      isVedAssist,
      type,
      sum,
      dueDate,
      customerCode,
      newEvents,
      newDocuments,
      ticketText,
      assignedUser
    };
  });
}

class PaymentTimelineService {
  userFilter$: Subject<UserFilter> = new Subject();
  offset$: BehaviorSubject<number> = new BehaviorSubject(0);
  search$: BehaviorSubject<string> = new BehaviorSubject('');

  reset$: Subject<void> = new Subject();
  isLoading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  hasMore$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  loadedEvents$: BehaviorSubject<Array<IPaymentEvent>> = new BehaviorSubject<Array<IPaymentEvent>>([]);

  subscription: Subscription = new Subscription();

  startMonitoring(): void {
    const withoutOffset$ = combineLatest([
      this.userFilter$.pipe(debounceTime(300), distinctUntilChanged()),
      this.search$.pipe(debounceTime(300), distinctUntilChanged())
    ]).pipe(
      tap(() => {
        this.isLoading$.next(true);
        this.offset$.next(0);
        this.loadedEvents$.next([]);
        this.reset$.next();
      })
    );

    const combinedResult$ = combineLatest([withoutOffset$, this.offset$])
      .pipe(
        debounceTime(300),
        map(val => {
          const [[userFilter, search], offset] = val;
          return { userFilter, search, offset };
        })
      );

    const filteringChange$ = combinedResult$
      .pipe(
        filter(v => !!v.userFilter || v.offset || v.search),
        switchMap((request: TLoadPaymentRequest) => defer(() => this.loadTimeline(request))),
        map((events: Array<PaymentEvent.AsObject>) => BuildPaymentEvents(events)),
        tap(() => this.isLoading$.next(false)),
        tap((events: Array<IPaymentEvent>) => this.loadedEvents$.next(events)),
        tap((events: Array<IPaymentEvent>) => this.hasMore$.next(events.length >= 20))
        // tap((events: IPaymentEvent[]) => this.hasMore$.next(false))
      );

    this.subscription = filteringChange$.subscribe();
  }

  stopMonitoring(): void {
    this.loadedEvents$.next([]);
    this.subscription && this.subscription.unsubscribe();
  }

  async loadTimeline(params: TLoadPaymentRequest): Promise<Array<PaymentEvent.AsObject>> {
    const request: IFilteredRequest = {
      search: params.search,
      userFilter: params.userFilter,
      offset: params.offset,
      group: false,
      empty: false,
      isOnlyMine: false
    };

    const { response } = await getPaymentTimelineEvents(request);

    if (!response) return [];

    return response.toObject().paymentsList;
  }
}

export const paymentTimelineService = new PaymentTimelineService();
