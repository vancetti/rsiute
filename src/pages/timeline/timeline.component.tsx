import React from 'react';
import { Panel } from 'rsuite';
import { userFiltersService } from 'services/user-filters.service';
import { useObservable } from 'utils/hooks';

export function AppTimeline(): JSX.Element {
  const filters = useObservable(userFiltersService.userFilters$);

  return (
    <Panel header="Список событий" shaded>
      {filters && filters.map(filter => <div key={filter.id}>{filter.name}</div>)}
    </Panel>
  );
}
