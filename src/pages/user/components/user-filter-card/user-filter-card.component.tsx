import { UserFilter } from '@tochka-modules/ved-proto/stash.bank24.int/ved/supreme/proto/user_filter_pb';
import { TDataItemType } from 'common/interfaces';
import React from 'react';
import {
  Checkbox,
  Col,
  FlexboxGrid,
  Grid,
  Icon,
  IconButton,
  Input,
  Loader,
  Panel,
  Row,
  TagPicker,
  Tooltip,
  Whisper
} from 'rsuite';
import { combineLatest, Subject } from 'rxjs';
import { debounceTime, filter, map, skip, startWith, takeUntil } from 'rxjs/operators';
import { userFiltersService } from 'services/user-filters.service';

import './user-filter-card.component.css';

interface IUserFilterCardProps {
    filter: UserFilter.AsObject;
    onGenerateClick: (filterId: number) => void;
}

interface IUserFilterCardState {
    isCurrentFilterInLoad: boolean;
}

export class UserFilterCard extends React.Component<IUserFilterCardProps, IUserFilterCardState> {
    get initialPrefixes(): Array<string> {
        if (!this.props.filter.prefixes) return [];
        return this.props.filter.prefixes.split(',');
    }

    get prefixes(): Array<string> {
      if (!this.props.filter.prefixes) return [];
      return this.props.filter.prefixes.split(',');
    }

    set prefixes(value: Array<string>) {
      this.prefixes$.next(Array.from(new Set([...value.map(v => v.trim().toUpperCase())])));
    }

    get isVedAssistant(): boolean {
      return this.props.filter.vedAssistant;
    }

    set isVedAssistant(isVed: boolean) {
      this.isVed$.next(isVed);
    }

    get isVip(): boolean {
      return this.props.filter.includeVip;
    }

    set isVip(isVip: boolean) {
      this.isVip$.next(isVip);
    }

    get filterName(): string {
      return this.props.filter.name;
    }

    set filterName(value: string) {
      this.filterName$.next(value);
    }

    get prefixData(): Array<TDataItemType> {
    return this.prefixes.map((prefix => ({
          label: prefix,
          value: prefix
        }
      )
    ));
  }

    readonly state: IUserFilterCardState = {
    isCurrentFilterInLoad: false
  };

    unsub$: Subject<void> = new Subject();

    prefixes$: Subject<Array<string>> = new Subject();
    isVed$: Subject<boolean> = new Subject();
    isVip$: Subject<boolean> = new Subject();
    filterName$: Subject<string> = new Subject();

    componentDidMount(): void {
      userFiltersService.filtersInLoad$
        .pipe(
          takeUntil(this.unsub$),
          map(filters => filters.indexOf(this.props.filter.id) > -1)
        )
        .subscribe(isCurrentFilterInLoad => this.setState({ isCurrentFilterInLoad }));

      this.initSubscription();
    }

    componentWillUnmount(): void {
      this.unsub$.next();
      this.unsub$.complete();
    }

    handlePrefixesChange = (value: Array<string>) => {
      this.prefixes = value;
    };

    initSubscription = () => {
      userFiltersService.generatedPrefixes$
        .pipe(filter(val => val.filterId === this.props.filter.id))
        .subscribe(val => this.prefixes = [...this.prefixes, ...val.prefixes]);

      const result$ = combineLatest([
        this.isVed$.pipe(startWith(this.isVedAssistant), map(val => Boolean(val))),
        this.isVip$.pipe(startWith(this.isVip), map(val => Boolean(val))),
        this.prefixes$.pipe(
          startWith(this.prefixes),
          map(prefixes => prefixes.join(',')),
          debounceTime(1000)
        ),
        this.filterName$.pipe(startWith(this.filterName), filter(v => !!v))
      ])
        .pipe(
          debounceTime(1000),
          skip(1),
          map(val => ({ includeVip: val[1], vedAssistant: val[0], prefixes: val[2], name: val[3] }))
        );

      result$
        .pipe(takeUntil(this.unsub$))
        .subscribe(value => userFiltersService.updateFilter(this.props.filter.id, value as UserFilter.AsObject));
    };

    handleDelete = () => userFiltersService.deleteFilter(this.props.filter.id);

    handleGenerate = () => this.props.onGenerateClick(this.props.filter.id);

    handleFilterNameChange = (value: string) => this.filterName = value;

    handleVipChange = (val: boolean) => this.isVip = val;

    handleVedAssistantChange = (val: boolean) => this.isVedAssistant = val;

    render(): JSX.Element {
      const { isCurrentFilterInLoad } = this.state;

      return (
        <Col xs={8}>
          <Panel shaded>
            <Grid fluid className="user-filter-card__grid">
              {isCurrentFilterInLoad && <Loader style={{ zIndex: 10 }} vertical size="md" center backdrop/>}
              <Row>
                <Input
                  defaultValue={this.filterName}
                  onChange={this.handleFilterNameChange}
                />
              </Row>
              <Row>
                <TagPicker
                  data={this.prefixData}
                  onChange={this.handlePrefixesChange}
                  menuStyle={{ display: 'none' }}
                  defaultValue={this.prefixes}
                  block
                  creatable
                  cleanable={false}
                />
              </Row>

              <Row>
                <Checkbox
                  onChange={this.handleVipChange}
                  value={this.isVip}
                  defaultChecked={this.isVip}
                >
                  VIP
                </Checkbox>
                <Checkbox
                  onChange={this.handleVedAssistantChange}
                  value={this.isVedAssistant}
                  defaultChecked={this.isVedAssistant}
                >
                  ВЭД-ассистент
                </Checkbox>
              </Row>
              <Row>
                <FlexboxGrid justify={'end'}>
                  <Whisper
                    delay={300}
                    placement="top"
                    trigger="hover"
                    speaker={<Tooltip>Сгенерировать фильтры</Tooltip>}
                  >
                    <IconButton
                      size={'lg'}
                      onClick={this.handleGenerate}
                      color="blue"
                      icon={<Icon icon={'cog'}/>}
                      style={{ marginRight: 10 }}
                    />
                  </Whisper>

                  <Whisper
                    delay={300}
                    placement="top"
                    trigger="hover"
                    speaker={<Tooltip>Удалить фильтр</Tooltip>}
                  >
                    <IconButton
                      size={'lg'}
                      onClick={this.handleDelete}
                      color="red"
                      icon={<Icon icon={'trash-o'}/>}
                    />
                  </Whisper>
                </FlexboxGrid>
              </Row>
            </Grid>
          </Panel>
        </Col>
      );
    }
}
