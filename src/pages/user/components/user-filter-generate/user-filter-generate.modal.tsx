import { InputCounter } from 'common/components/input-counter';
import React, { useState } from 'react';
import { Button, Col, Grid, Modal } from 'rsuite';

import { userFiltersService } from 'services/user-filters.service';
import './user-filter-generate.modal.styl';

interface IUserFilterGenerateProps {
  isVisible: boolean;
  filterId: number;
  onClose: () => void;
}

function createRange(start: string, stop: string): Array<string> {
  const result: Array<string> = [];
  for (let idx = start.charCodeAt(0), end = stop.charCodeAt(0); idx <= end; ++idx) {
    result.push(String.fromCharCode(idx));
  }
  return result.length > 1 ? result.map(item => item.toUpperCase()) : [start[0]];
}

export function UserFilterGenerateModal(props: IUserFilterGenerateProps): JSX.Element {
  const { isVisible, onClose, filterId } = props;
  const [fromStr, setFromString] = useState('');
  const [toStr, setToString] = useState('');
  const [hasError, setError] = useState(false);

  const checkForErrors = () => {
    const hasErrors = (
      !fromStr
      || !toStr
      || (fromStr.length !== toStr.length)
      || (fromStr.toUpperCase().length > 4)
      || (toStr.toUpperCase() < fromStr.toUpperCase())
    );
    setError(hasErrors);
    return hasErrors;
  };

  const handleClose = () => {
    setError(false);
    setFromString('');
    setToString('');
    onClose();
  };

  const handleSubmit = () => {
    if (checkForErrors()) return;

    const rangeTable: Record<string, Array<string>> = {};

    const fromStrToUpper = fromStr.toUpperCase();
    const toStringToUpper = toStr.toUpperCase();

    fromStrToUpper
      .split('')
      .forEach((letter, index) => rangeTable[index] = createRange(letter, toStringToUpper));

    const prefixes = Object.values(rangeTable).reduce((acc, curr) => {
      const a: Array<string> = [];
      (acc as Array<string>)
        .forEach(item =>
          (curr as Array<string>).forEach(currItem => a.push(item + currItem)));
      return a;
    });

    userFiltersService.generatedPrefixes$.next({ filterId, prefixes });
    handleClose();
  };

  return (
    <Modal size="xs" show={isVisible} onHide={onClose}>
      <Modal.Header>
        <Modal.Title>Сгенерировать фильтры</Modal.Title>
      </Modal.Header>
      <Modal.Body className="user-filter-generate-modal__body">
        <Grid fluid>
          <Col className="user-filter-generate-modal__row" xs={24}>
            Используя данную форму можно сгенерировать фильтры автоматически, используя диапазон букв.
          </Col>

          <Col className="user-filter-generate-modal__row" xs={24}>
            Фильтры добавятся к уже существующим.
          </Col>

          <Col xs={24} className="user-filter-generate-modal__row">
            <Grid style={{ paddingLeft: 0 }} className={hasError ? 'user-filter-generate-modal--error' : ''} fluid>
              <Col xs={11}>
                <InputCounter
                  inputProps={{ placeholder: 'С', onChange: setFromString, value: fromStr }}
                  maxCounter={4}
                />
              </Col>

              <Col xsPush={2} xs={11}>
                <InputCounter
                  inputProps={{ placeholder: 'По', onChange: setToString, value: toStr }}
                  maxCounter={4}
                />
              </Col>
            </Grid>
          </Col>

                </Grid>
            </Modal.Body>
            <Modal.Footer>
                <Button appearance="primary" onClick={handleSubmit}>
                    Добавить
                </Button>
                <Button onClick={handleClose} appearance="subtle">
                    Отменить
                </Button>
            </Modal.Footer>
        </Modal>
    );
}
