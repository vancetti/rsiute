import { UserFilterCard } from 'pages/user/components';
import { UserFilterGenerateModal } from 'pages/user/components/user-filter-generate';
import React, { useCallback, useState } from 'react';
import { Button, FlexboxGrid, Grid, Loader, Panel } from 'rsuite';
import { authService } from 'services';
import { userFiltersService } from 'services/user-filters.service';
import { useObservable } from 'utils/hooks';

interface IGeneratorModalProps {
  filterId: number;
  isVisible: boolean;
}

export function UserPage(): JSX.Element {
  const isLoading = useObservable(userFiltersService.isLoading$);
  const filters = useObservable(userFiltersService.userFilters$);
  const addNewFilter = useCallback(() => userFiltersService.addNewFilter(), []);
  const currentUser = useObservable(authService.currentUser$);

  const [generatorModalState, updateGeneratorModalState] =
    useState<IGeneratorModalProps>({ isVisible: false, filterId: 0 });

  const handleModalVisibility = ({ filterId, isVisible }: IGeneratorModalProps) => {
    updateGeneratorModalState({ filterId, isVisible });
  };

  const renderPanelHeader = (): JSX.Element => {
    return (
      <FlexboxGrid justify={'space-between'}>
        <FlexboxGrid.Item xs={4}>
          {currentUser ? currentUser.fullName : ''}
        </FlexboxGrid.Item>

        <FlexboxGrid.Item xs={4}>
          <Button onClick={addNewFilter} color="green">Добавить новый фильтр</Button>
        </FlexboxGrid.Item>
      </FlexboxGrid>
    );
  };

  return (
    <Panel header={renderPanelHeader()} shaded>
      {isLoading && <Loader size="md" vertical/>}

      {!isLoading && <Grid fluid>
        {filters && filters
          .map(filter =>
            <UserFilterCard
              onGenerateClick={handleModalVisibility.bind(null, { isVisible: true, filterId: filter.id })}
              filter={filter}
              key={filter.id}
            />)}
      </Grid>}

      <UserFilterGenerateModal
        {...generatorModalState}
        onClose={handleModalVisibility.bind(null, { filterId: 0, isVisible: false })}
      />
    </Panel>
  );
}
