import { CustomerPage } from 'pages/customer';
import { AppTimeline } from 'pages/timeline';
import { UserPage } from 'pages/user';
import React from 'react';
import { Route, Switch } from 'react-router-dom';

export function AppRoutes(): JSX.Element {
  return (
    <Switch>
      <Route exact component={AppTimeline} path={'/'}/>
      <Route component={UserPage} path={'/user'}/>
      <Route component={CustomerPage} path={'/customer/:id'}/>
    </Switch>
  );
}
