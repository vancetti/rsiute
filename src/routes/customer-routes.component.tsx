import React from 'react';
import { Route, Switch } from 'react-router-dom';

function MockComponent(): JSX.Element {
    return (
      <div>mock</div>
    );
}

export function CustomerRoutes(): JSX.Element {
  return (
    <Switch>
      <Route component={MockComponent} path={'contracts'}/>
    </Switch>
  );
}
