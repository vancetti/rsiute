import { User } from '@tochka-modules/ved-proto/stash.bank24.int/ved/supreme/proto/user_pb';
import { loadUser } from 'api';
import { IFetchDataResponse } from 'api/grpc/grpcToPromise';
import { BehaviorSubject, from, Observable, Subscription, timer } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';

class AuthService {
    private user$: BehaviorSubject<User.AsObject> = new BehaviorSubject(new User().toObject());
    private subscription: Subscription = new Subscription();

    isLoading$: BehaviorSubject<boolean> = new BehaviorSubject(Boolean(1));
    currentUser$: Observable<User.AsObject> = this.user$.asObservable();

    get currentUser(): User.AsObject {
        return this.user$.getValue();
    }

    makeObservableRequest(): Observable<User> {
        return timer(0, 30000)
          .pipe(
            tap(() => this.isLoading$.next(true)),
            switchMap(() => from(loadUser())),
            tap(() => this.isLoading$.next(false)),
            map((rawResponse: IFetchDataResponse<User>) => rawResponse.response || new User()),
            tap((user: User) => this.user$.next(user.toObject()))
          );
    }

    startMonitoringUser(): void {
        this.subscription = this.makeObservableRequest().subscribe();
    }

    stopMonitoringUser(): void {
        this.subscription.unsubscribe();
    }
}

export const authService = new AuthService();
