export class CacheService {
    private static FIELD_KEY: string = 'ved-supreme';
    private static LAST_CUSTOMERS_KEY: string = 'last-customers';

    setItem(key: string | number, value: string): void {
        const { FIELD_KEY } = CacheService;

        localStorage.setItem(`${FIELD_KEY}:${key}`, value);
    }

    getItem(key: string): string | null {
        const { FIELD_KEY } = CacheService;
        return localStorage.getItem(`${FIELD_KEY}:${key}`);
    }

    getLastCustomerId(): number {
        const result = this.getItem('last-customer-id');
        return result ? Number(result) : -1;
    }

    setLastCustomerId(id: string | number): void {
        this.setItem('last-customer-id', id.toString());
    }

    getLastCustomersIds(): string | null {
        const { LAST_CUSTOMERS_KEY } = CacheService;
        return this.getItem(LAST_CUSTOMERS_KEY);
    }

    updateLastCustomersIds(newCustomerIds: Array<number>): void {
        const { LAST_CUSTOMERS_KEY } = CacheService;
        this.setItem(LAST_CUSTOMERS_KEY, JSON.stringify(newCustomerIds));
    }
}

export const cacheService = new CacheService();
