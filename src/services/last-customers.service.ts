import {
    Customer,
    GetCustomersByIdsResponse
} from '@tochka-modules/ved-proto/stash.bank24.int/ved/supreme/proto/customer_pb';
import { getCustomersByIds } from 'api/customers';
import { IFetchDataResponse } from 'api/grpc/grpcToPromise';
import { BehaviorSubject, from, Subscription } from 'rxjs';
import { filter, map, skip, switchMap, tap } from 'rxjs/operators';
import { cacheService } from 'services/cache.service';

class LastCustomersService {
    private static LAST_CUSTOMERS_COUNT: number = 5;

    private lastCustomersCounter: number = 0;
    private subscription: Subscription = new Subscription();

    lastCustomerId$: BehaviorSubject<number> = new BehaviorSubject(-1);
    lastCustomersIds$: BehaviorSubject<Array<number>> = new BehaviorSubject([-1]);
    lastCustomers$: BehaviorSubject<Array<Customer.AsObject>> = new BehaviorSubject([new Customer().toObject()]);

    setLastCustomerId(newCustomerId: number): void {
        this.lastCustomerId$.next(newCustomerId);
        cacheService.setLastCustomerId(newCustomerId);
        this.updateLastCustomersStorage(newCustomerId);
    }

    getCachedIds(): Array<number> {
        const result = cacheService.getLastCustomersIds();
        return result ? JSON.parse(result) : [];
    }

    startMonitoring(): void {
        this.subscription = this.lastCustomersIds$
          .pipe(
            skip(1),
            switchMap((lastIds: Array<number>) => from(getCustomersByIds(lastIds))),
            filter((rawResponse: IFetchDataResponse<GetCustomersByIdsResponse>) => !!rawResponse.response),
            map(rawResponse => rawResponse.response ? rawResponse.response.toObject().customersList : []),
            tap((customers: Array<Customer.AsObject>) => this.lastCustomers$.next(customers))
          )
          .subscribe();

        this.lastCustomersIds$.next(this.getCachedIds());
        this.lastCustomerId$.next(cacheService.getLastCustomerId());
    }

    stopMonitoring(): void {
        this.subscription.unsubscribe();
    }

    updateLastCustomersStorage(newCustomerId: number): void {
        const { LAST_CUSTOMERS_COUNT } = LastCustomersService;
        const storage = cacheService.getLastCustomersIds();
        let result: Array<number> = [newCustomerId];

        if (storage) {
            const lastCustomers: Array<number> = JSON.parse(storage);

            if (lastCustomers.length < LAST_CUSTOMERS_COUNT && !lastCustomers.includes(newCustomerId)) {
                lastCustomers.push(newCustomerId);
            } else if (!lastCustomers.includes(newCustomerId)) {
                lastCustomers.splice(this.lastCustomersCounter, 1, newCustomerId);
                this.lastCustomersCounter++;
                // Обновляем счетчик в случае, если он равен кол-ву необходимо хранимых последних просмотренных клиентов
                (this.lastCustomersCounter === LAST_CUSTOMERS_COUNT) && (this.lastCustomersCounter = 0);
            }
            result = lastCustomers;
        }
        cacheService.updateLastCustomersIds(result);
        this.lastCustomersIds$.next(result);
    }
}

export const lastCustomersService = new LastCustomersService();
