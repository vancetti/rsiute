import { UserFilter } from '@tochka-modules/ved-proto/stash.bank24.int/ved/supreme/proto/user_filter_pb';
import { createUserFilter, deleteFilterById, getFiltersByUser, updateFilter } from 'api';
import { BehaviorSubject, defer, from, Observable, Subject } from 'rxjs';
import { map, skipWhile, switchMap, take, tap } from 'rxjs/operators';
import { authService } from 'services/auth.service';

type TUserFilterStore = BehaviorSubject<Array<UserFilter.AsObject>>;

class UserFiltersService {
    private userFiltersStore$: TUserFilterStore = new BehaviorSubject([new UserFilter().toObject()]);

    userFilters$: Observable<Array<UserFilter.AsObject>> = this.userFiltersStore$.asObservable();
    filtersInLoad$: BehaviorSubject<Array<number>> = new BehaviorSubject([-1]);
    isLoading$: BehaviorSubject<boolean> = new BehaviorSubject(Boolean(1));

    generatedPrefixes$: Subject<{ filterId: number; prefixes: Array<string> }> = new Subject();

    loadFilters(): void {
        from(authService.currentUser$)
          .pipe(
            skipWhile(user => !user.id),
            take(1),
            tap(() => this.isLoading$.next(true)),
            map(user => user.id),
            switchMap(id => defer(() => from(getFiltersByUser(id)))),
            map(rawResponse => rawResponse.response ? rawResponse.response.toObject().resultsList : []),
            tap(filters => this.userFiltersStore$.next(filters)),
            tap(() => this.isLoading$.next(false))
          ).subscribe();
    }

    getFilterById(id: number): UserFilter.AsObject | undefined {
        return this.userFiltersStore$.getValue().find(filter => filter.id === id);
    }

    addNewFilter(): void {
        this.isLoading$.next(true);
        createUserFilter(authService.currentUser.id)
          .then(rawResponse => {
              if (rawResponse.response && rawResponse.response.getResult()) {
                  const result = rawResponse.response.getResult();
                  if (!result) return;
                  this.userFiltersStore$.next([...this.userFiltersStore$.getValue(), result.toObject()]);
              }
          })
          .then(() => this.isLoading$.next(false));
    }

    toggleFilterInLoad(id: number): void {
        const currentFiltersInLoad = this.filtersInLoad$.getValue();
        const filterIndex = currentFiltersInLoad.indexOf(id);
        if (filterIndex > -1) {
            currentFiltersInLoad.splice(filterIndex, 1);
            this.filtersInLoad$.next(currentFiltersInLoad);
        } else {
            this.filtersInLoad$.next([...currentFiltersInLoad, id]);
        }
    }

    updateFilter(id: number, payload: UserFilter.AsObject): void {
        const filter = new UserFilter();
        const userId = authService.currentUser.id;
        filter.setUserId(userId);
        filter.setName(payload.name);
        filter.setId(id);
        filter.setIncludeVip(payload.includeVip);
        filter.setPrefixes(payload.prefixes);
        filter.setVedAssistant(payload.vedAssistant);

        this.toggleFilterInLoad(id);

        updateFilter(filter)
          .then(() => {
              const currentState = this.userFiltersStore$.getValue();
              const indexToUpdate = currentState.findIndex(f => f.id === id);
              currentState.splice(indexToUpdate, 1, filter.toObject());
              this.userFiltersStore$.next(currentState);

              this.toggleFilterInLoad(id);
          });
    }

    addPrefixesToFilter(id: number, prefixes: Array<string>): void {
        const joinedPrefixes = prefixes.join(',');
        const filter = this.getFilterById(id);
        if (!filter) return;

        filter.prefixes = joinedPrefixes;
        this.updateFilter(id, filter);
    }

    deleteFilter(id: number): void {
        this.toggleFilterInLoad(id);
        deleteFilterById(id).then(({ response }) => {
            if (!response) return;
            const currentFilters = this.userFiltersStore$.getValue();
            this.toggleFilterInLoad(id);

            this.userFiltersStore$.next(currentFilters.filter(f => f.id !== id));
        });
    }
}

export const userFiltersService = new UserFiltersService();
