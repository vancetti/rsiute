const proxy = require('http-proxy-middleware');

const defaultProxySettings = proxy({
    target: process.env.REACT_APP_API,
    changeOrigin: true,
    secure: false
});

const endpoints = [
    'api',
    'auth',
    'check',
    'authorize',
    'files'
];

module.exports = function (app) {
    endpoints.forEach(endpoint => app.use(`/${endpoint}`, defaultProxySettings));
};
