import { Filtering, LogicalOperator, NullCondition, NumberCondition, StringCondition } from '@tochka-modules/ved-proto';
import { FieldMask } from 'google-protobuf/google/protobuf/field_mask_pb';
import { grpc } from 'grpc-web-client';
import { UnaryOutput } from 'grpc-web-client/dist/typings/unary';
import { stringify } from 'querystring';

function authenticate() {
    const params = {
        client_id: process.env.REACT_APP_CLIENT_ID,
        redirect_uri: process.env.REACT_APP_REDIRECT_URI,
        response_type: 'code',
        scope: 'default',
        state: '8RxtMkKVpZALmtqf'
    };

    window.location.assign(
        `${process.env.REACT_APP_AUTH_ENDPOINT}/authorize?${stringify(params)}`
    );
}

export const isProd = process.env.REACT_APP_ENV === 'prod';

interface IUnaryResponse {
    cancel(): void;
}

type CanceledRequest<IResponse> = Promise<IResponse> & IUnaryResponse;

interface IFetchData<IData, IResult> {
    transport: (value: IData) => Promise<IResult>;
    data?: IData;
    showInformer?: boolean;
}

interface IFetchDataCanceled<IData, IResult> {
    transport: (value: IData) => CanceledRequest<IResult>;
    data?: IData;
    showInformer?: boolean;
}

export interface IFetchDataResponse<T = any> {
    isSuccess: boolean;
    response?: T;
    error?: UnaryOutput<T>;
}

export const DEFAULT_ERROR_MESSAGE = 'Ошибка сервера. Попробуйте чуть позже.';

export const extractMessageFromError = <TResponse>(
    error: UnaryOutput<TResponse>
) => {
    if (error.statusMessage) {
        return error.statusMessage;
    }

    return DEFAULT_ERROR_MESSAGE;
};

export function fetchDataCanceled<IData, IResult>({
                                                    transport,
                                                    data
                                                  }: IFetchDataCanceled<IData, IResult>): CanceledRequest<IFetchDataResponse<IResult>> {
  const executedTransport = transport(data!);
  const promise = executedTransport
    .then(response => ({
      isSuccess: true,
      response
    }))
    .catch(error => {
            if (error.code === grpc.Code.Unauthenticated) {
                authenticate();
            }

            if (!isProd) {
                // tslint:disable-next-line:no-console
                console.error(error);
            }

            return {
                isSuccess: false,
                error
            };
        });
    (promise as CanceledRequest<IFetchDataResponse<IResult>>).cancel = () =>
        executedTransport.cancel();
    return promise as CanceledRequest<IFetchDataResponse<IResult>>;
}

export const fetchData =
    async <IData, IResult>({
                               transport,
                               data
                           }: IFetchData<IData, IResult>):
        Promise<IFetchDataResponse<IResult>> => {
        try {
            const response = await transport(data!);

            return {
                isSuccess: true,
                response
            };
        } catch (error) {
            if (error.code === grpc.Code.Unauthenticated) {
                authenticate();
            }

            if (error.code === grpc.Code.PermissionDenied) {
                return {
                    isSuccess: false,
                    error
                };
            }

            if (!isProd) {
                // tslint:disable-next-line:no-console
                console.error(error);
            }

            return {
                isSuccess: false,
                error
            };
        }
    };

interface IServiceError {
    message: string;
    code: number;
    metadata: grpc.Metadata;
}

export function grpcToPromise<IRequest, IResponse>(
    service: (
        arg1: IRequest,
        arg2: (err: IServiceError | null, res: IResponse | null) => void
    ) => IUnaryResponse
) {
    return (request: IRequest): CanceledRequest<IResponse> => {
        let unaryResponse: IUnaryResponse;
        const promised = new Promise<IResponse | null>((resolve, reject) => {
            unaryResponse = service(request, (err, res) => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve(res);
            });
        });
        (promised as CanceledRequest<IResponse>).cancel = () =>
            unaryResponse.cancel();

        return promised as CanceledRequest<IResponse>;
    };
}

export const makeNumberFiltering = (cond: NumberCondition): Filtering => {
    const filter = new Filtering();
    filter.setNumberCondition(cond);
    return filter;
};

export const makeStringFiltering = (cond: StringCondition): Filtering => {
    const filter = new Filtering();
    filter.setStringCondition(cond);
    return filter;
};

export const makeOperatorFiltering = (cond: LogicalOperator): Filtering => {
    const filter = new Filtering();
    filter.setOperator(cond);
    return filter;
};

export const makeNullFiltering = (cond: NullCondition): Filtering => {
    const filter = new Filtering();
    filter.setNullCondition(cond);
    return filter;
};

export const makeFieldMask = (...fields: Array<string>): FieldMask => {
  const mask = new FieldMask();
  mask.setPathsList(fields);
  return mask;
};
