import { Customer } from '@tochka-modules/ved-proto/stash.bank24.int/ved/supreme/proto/customer_pb';
import { getCustomer } from 'api';
import { useEffect, useState } from 'react';

export function useCustomer(id: number | string): Customer.AsObject {
  const [customer, setCustomer] = useState<Customer.AsObject>(new Customer().toObject());
  const customerId = Number(id);

  useEffect(() => {
    getCustomer(customerId)
      .then((c: Customer) => setCustomer(c.toObject()));
  }, [id, customerId]);

  return customer;
}
