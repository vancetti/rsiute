import { Observable } from 'rxjs';
import { useObservable } from './use-observable';

export const useLoadableObservable = <S, L = boolean, E = Error | null>(
    source: Observable<S>,
    loading: Observable<L>,
    error: Observable<E>
): [S | undefined, L | undefined, E | undefined] => [
    useObservable(source),
    useObservable(loading),
    useObservable(error)
];
