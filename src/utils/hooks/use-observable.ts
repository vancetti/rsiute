import { useEffect, useState } from 'react';
import { Observable } from 'rxjs';

export function useObservable<T>(value$: Observable<T>, initialValue?: T): T | undefined {
    const [value, setValue] = useState<T | undefined>(initialValue);

    useEffect(() => {
        const subscription = value$.subscribe(setValue);

        return () => subscription.unsubscribe();
    }, [value$]);

    return value;
}
