const prettyCustomerName = [
    ['индивидуальный предприниматель', 'ИП'],
    ['общество с ограниченной ответственностью', 'ООО'],
    ['открытое акционерное общество', 'ОАО'],
    ['закрытое акционерное общество', 'ЗАО'],
    ['публичное акционерное общество', 'ПАО'],
    ['публичного акционерного общества', 'ПАО'],
    ['инспекция федеральной налоговой службы', 'ИФНС'],
    ['управление федерального казначейства', 'УФК'],
    ['российской федерации', 'РФ'],
    ['государственное учреждение', 'ГУ'],
    ['фонда социального страхования', 'ФСС'],
    ['федеральное государственное унитарное предприятие', 'ФГУП'],
    ['государственное унитарное предприятие', 'ГУП'],
    ['муниципальное унитарное предприятие', 'МУП'],
    ['сельскохозяйственный кооператив', 'СХК'],
    ['общество с дополнительной ответственностью', 'ОДО'],
    ['общественная организация', 'ОО'],
    ['некоммерческое партнерство', 'НП'],
    ['муниципальное учреждение', 'МУч'],
    ['общественное учреждение', 'ОУч'],
    [
        'автономная некоммерческая организация дополнительного профессионального образования',
        'АНО ДПО'
    ],
    ['автономная некоммерческая организация', 'АНО'],
    ['товарищество собственников жилья', 'ТСЖ'],
    ['общественное движение', 'ОД'],
    ['общественный фонд', 'ОФ'],
    ['товарищество с ограниченной ответственностью', 'ТОО'],
    ['государственное предприятие', 'ГП'],
    ['муниципальное предприятие', 'МУП'],
    ['жилищно-строительный кооператив', 'ЖСК'],
    ['гаражно-строительный кооператив', 'ГСК'],
    ['научно-производственное объединение', 'НПО'],
    ['паевой инвестиционный фонд', 'ПИФ'],
    ['негосударственный пенсионный фонд', 'НПФ'],
    ['ремонтно-строительное управление', 'РСУ'],
    ['муниципальное автономное учреждение', 'МАУ'],
    ['муниципальное бюджетное учреждение', 'МБУ'],
    ['государственное автономное учреждение', 'ГАУ'],
    ['небанковская кредитная организация', 'НКО'],
    ['негосударственное образовательное учреждение', 'НОУ'],
    ['акционерное общество', 'АО'],
    ['научно-производственное предприятие', 'НПП'],
    ['ХАНТЫ-МАНСИЙСКИЙ БАНК ОТКРЫТИЕ', 'Ханты-Мансийский Банк Открытие'],
    ['транспортная компания', 'ТК']
];

export const prettifyCustomerName = (customerFullName: string): string => {
    let customerName = '';
    for (const [query, prettyName] of prettyCustomerName as Array<Array<string>>) {
        const re = new RegExp(`(${query})`, 'gi');
        if (re.test(customerFullName)) {
            customerName = customerFullName.replace(re, prettyName);
        }
    }
    return customerName;
};
